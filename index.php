<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="fui/semantic.css">
		<script src="fui/semantic.js"></script>
	</head>
	<body>
		<div class="ui main text container">
		<h1>Gérer son compte Scenari</h1>
		<div class="ui segments">
			<article class="ui segment">
				<h2 class="ui header">Activer son compte</h2>
				<p>Cette action permet aux utilisateurs de l'UTC d'activer leur compte sur le SCENARIserver<!-- et de nouveaux ateliers--></p>
				<a class="ui fluid submit button" href="manager.php?self=true">Activer son compte</a>
			</article>
			<article class="ui segment">
				<h2 class="ui header">Inviter des utilisateurs</h2>
				<p>Si vous souhaitez inviter des utilisateurs Scenari à travailler sur votre projet :</p>
				<ul>
					<li><b>Pour les utilisateurs UTC</b> : donnez leur seulement le lien vers cette page pour qu'ils activent eux même leur compte. Leur compte est leur login UTC.</li>
					<li><b>Pour les utilisateurs extérieurs</b> : vous pouvez créer un compte pour eux, en complétant le formulaire ci-dessous. Assurez vous qu'un compte n'existe pas déjà pour eux (format prenom.nom). Ils recevrons un email avec un mot de passe temporaire</li>
					<form class="ui form" method="post" action="manager.php">
						<div class="field"><label><span class="labeltext">Prénom :</span> <input type="text" name="addfirstname"/></label></div>
						<div class="field"><label><span class="labeltext">Nom :</span> <input type="text" name="addlastname"/></label></div>
						<div class="field"><label><span class="labeltext">Email :</span> <input type="email" name="addemail"/></label></div>
						<div><input class="ui button" type="submit" value="Créer l'utilisateur extérieur à l'UTC"></div>
					</form>
				</ul>
				<p>Puis, depuis l'écran de gestion des droits de votre atelier, ajoutez les droits aux nouveaux utilisateurs.</p>
				<a class="ui fluid submit button" href="/help/">Documentation</a>
			</article>
			<article class="ui segment">
				<h2 class="ui header">Se faire aider par un humain</h2>
				<p>Porteurs de projet de création de contenus : la CAP peut vous aider...
				<ul>
					<li>Intervention projet : étude des besoins, formation, accompagnement*</li>
					<li>Intervention technique : renommer ou supprimer des ateliers, aider sur un soucis avec une manipulation</li>
				</ul>
				<p>* Ce service est limité par les disponibilités de nos équipes, nous serons amenés à répartir le temps accordé entre les différentes demandes et vous devrez investir du temps de votre coté pour être autonome sur vos projets.</p>
				</p>
				<a class="ui fluid submit button" href="mailto:stephane.poinsart@utc.fr">Assistance Mail</a>
				<a class="ui fluid submit button" href="https://team.picasoft.net/appui-pedago/">Assistance chat</a>
			</article>
		</div>
		</div>
	</body>
</html>
