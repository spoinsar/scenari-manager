<?php
require_once('config.php');


// before redirect to CAS, save the POST variables in a session
session_start();
if ($_GET['self']) {
	foreach (array('addfirstname', 'addlastname', 'addemail') as $v) {
		if (array_key_exists($v, $_SESSION)) {
			unset($_SESSION[$v]);
		}
	}
}
if ($_POST['addfirstname'] || $_POST['addlastname'] || $_POST['addemail']) {
	foreach (array('addfirstname', 'addlastname', 'addemail') as $v) {
		if (array_key_exists($v, $_POST)) {
			$_POST[$v]=trim($_POST[$v]);
			$_SESSION[$v]=$_POST[$v];
		}
	}
} else {
	foreach (array('addfirstname', 'addlastname', 'addemail') as $v) {
		if (array_key_exists($v, $_SESSION)) {
			$_POST[$v]=$_SESSION[$v];
		}
	}
}
session_write_close(); 


require_once('cas.php');
if (!$login) {
	die('Problème d\'authentification... Login / mot de passe incorrect ?');
}
$creator=$login;
$error=true;

// force user and group for test purposes
//$login='toto42';
// $cas_attributes['accountProfile']='utc-etu';
//print_r($cas_attributes);



$api_key=null;


function scformdata($ar) {
	$s='(';
	foreach ($ar as $key => $value) {
		$s.=$key;
		if ($value===false) {
			$s.='!F!';
		} elseif ($value===true) {
			$s.='!T!';
		} elseif (is_array($value)) {
			$s.='*';
			$a=array();
			foreach ($value as $subvalue) {
				$a[]="'".str_replace("'", "''", $subvalue)."'";
			}
			$s.=implode('.', $a);
			$s.="-";
		} else {
			$s.="'".str_replace("'", "''", $value)."'";
		}
	}
	$s.=')';
	return $s;
}


function callAPI($method, $url, $data=null){
	global $api_key, $SCENARI_URL, $SCENARI_USER, $SCENARI_PASSWORD;
	$SCENARI_LOGIN_URL='/public/u/loginWeb';

	// Whatever the request is, if we dont have an api_key, we need to login first with an admin account.
	// We force a first request to do that, then we add the recieved token to our real querries.
	if (!$api_key && $url!=$SCENARI_LOGIN_URL) {
		callAPI(
			'POST',
			'/public/u/loginWeb',
			array('userProps'=>scformdata(array('nickOrAccount'=>$SCENARI_USER, 'currentPwd'=>$SCENARI_PASSWORD)))
		);
	}
	$curl = curl_init();
	switch ($method) {
	case "POST":
		curl_setopt($curl, CURLOPT_POST, 1);
		if ($data)
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		break;
	case "PUT":
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
		if ($data)
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
		break;
	default:
		if ($data)
			$url = sprintf("%s?%s", $url, http_build_query($data));
	}
	$headers = [];
	curl_setopt($curl, CURLOPT_VERBOSE, true);
	curl_setopt($curl, CURLOPT_URL, $SCENARI_URL.$url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_HEADERFUNCTION,
		// decode the server headers
		function($curl, $header) use (&$headers) {
			$len = strlen($header);
			$header = explode(':', $header, 2);
			if (count($header) < 2) { // ignore invalid headers
				return $len;
			}

			$headers[strtolower(trim($header[0]))][] = trim($header[1]);

			return $len;
		}
	);
	$reqheaders=array(
		'ScCsrf: 1'
	);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $reqheaders);
	if ($api_key) {
// echo "set cookie: $api_key";
		curl_setopt($curl, CURLOPT_COOKIE, "scTk=$api_key");
	}

/*
if ($data) {
	echo "\n<br>Request: ".$method.", ".$url.", ".print_r($data, true)."<br>\n";
} else {
	echo "\n<br>Request: ".$method.", ".$url."<br>\n";
}*/
	// EXECUTE:
	$body=curl_exec($curl);
	if($body===false) {
		echo("Connection Failure: ".curl_error($curl).", msg: ".curl_error($curl));
		print_r(curl_getinfo($curl));
		if ($url==$SCENARI_LOGIN_URL) {
			echo "\nFATAL : erreur interne d'accès au SCENARIserver\n";
			exit;
		}

		return false;
	}
	curl_close($curl);
	if ($url==$SCENARI_LOGIN_URL) {
		if (isset($headers['set-cookie'][0]) && preg_match('/scTk=(.*?);/', $headers['set-cookie'][0], $match)===1) {
			$api_key=$match[1];
//			echo "\nset api key: $api_key\n";
		}
	}
/*echo "\nheaders:\n";
print_r($headers);
echo "\nbody:\n";
print_r($body);*/
	return $body;
}

// check auth
//callAPI('GET', "/web/u/adminUsers?cdaction=CurrentUser");

$status='';


/*
// test to add multiple users from batch array data
$we01=array(
array('login'=>'bonjean', 'email'=>'jean.bon@etu.utc.fr', 'firstname'=>'Jean', 'lastname'=>'Bon')
);

foreach ($we01 as $weuser) {
	$login=$weuser['login'];

	$scenariuser['account']=$weuser['login'];
	$scenariuser['firstName']=$weuser['firstname'];
	$scenariuser['lastName']=$weuser['lastname'];
	$scenariuser['email']=$weuser['email'];
print_r($scenariuser);

 */

$ret=null;
$error=false;
if ($_POST['addfirstname'] || $_POST['addlastname'] || $_POST['addemail']) {
	// create external user

	$genpwd=substr(str_shuffle("abcdefghijklmnopqrstuvwxyz123456789"),0,10);
	setlocale(LC_ALL, "en_US.utf8");
	$cleanfirstname=preg_replace("/[^a-z]+/", "", strtolower(iconv("utf-8", "ascii//TRANSLIT", $_POST['addfirstname'])));
	$cleanlastname=preg_replace("/[^a-z]+/", "", strtolower(iconv("utf-8", "ascii//TRANSLIT", $_POST['addlastname'])));
	$login=$cleanfirstname.'.'.$cleanlastname;

	if (! $_POST['addfirstname']) {
		$status='Prénom manquant : pour créer un utilisateur externe, vous devez compléter tous les champs du formulaire';
		$error=true;
	}
	if (! $_POST['addlastname']) {
		$status='Nom manquant : pour créer un utilisateur externe, vous devez compléter tous les champs du formulaire';
		$error=true;
	}
	if (! $_POST['addemail']) {
		$status='Addresse email manquante : pour créer un utilisateur externe, vous devez compléter tous les champs du formulaire';
		$error=true;
	}

	if (!$error) {
		$scenariuser=array(
			'account'	=> $login,
			'isHidden'	=> false,
			'groups'	=> array('all', 'ext', 'invite'),
			'firstName'	=> $_POST['addfirstname'],
			'lastName'	=> $_POST['addlastname'],
			'email'		=> $_POST['addemail'],
			'authMethod'	=> '',
			'password'	=> $genpwd,
			'confirmPwd'	=> $genpwd,
			'userType'	=> 'user'
		);
		$ret=callAPI('POST', "/web/u/createUser?param=".$login, array('userProps'=>scformdata($scenariuser)));
	}
} else {
	// create own user
	$scenariuser=array(
		'account'	=> $login,
		'isHidden'	=> false,
		'groups'	=> array('all', ($cas_attributes['accountProfile']=='utc-pers')?'utc-pers':'utc-etu'),
		'firstName'	=> $cas_attributes['givenName'],
		'lastName'	=> $cas_attributes['sn'],
		'email'		=> $cas_attributes['mail'],
		'authMethod'	=>'remote',
		'userType'	=>'user'
	);
	$ret=callAPI('POST', "/web/u/createUser?param=".$scenariuser['account'], array('userProps'=>scformdata($scenariuser)));
}
$reta=json_decode($ret, true);
if (is_array($reta)) {
	if ($reta['result']=='available') {
		$status.="Utilisateur \"$login\" activé avec succès\n";
		$error=false;
		if ($genpwd) {
			$to      = $_POST['addemail'];
			$subject = '=?UTF-8?B?' . base64_encode('Création de votre compte Scenari sur le serveur UTC') . '?=';
			$message = base64_encode("Bonjour,\n\n A la demande de ". $cas_attributes['givenName']." ".$cas_attributes['sn']. " <".$cas_attributes['mail'].">, un compte sur l'outil d'édition de documents Scenari de l'UTC a été créé pour vous. Pour vous connecter, vous pouvez utiliser les informations suivantes :\n\nURL : https://scenariutc.utc.fr/\nLogin : $login\nMot de passe : $genpwd\n\nSi vous rencontrez un problème technique pour accéder à votre compte, contactez la Cellule d'Appui Pédagogique (cap@utc.fr) qui assure une assistance technique sur ce service.\nPour tout autre problème (par exemple savoir sur quels documents travailler ou sur ce que vous devez faire dans le projet), contactez la personne qui a créé votre compte : ".$cas_attributes['mail']."\n\nEn vous souhaitant bonne utilisaiton.\nLa Cellule d'Appui Pédagogique,\nUniversité de Technologie de Compiègne\n\nCeci est un message envoyé automatiquement. ");
			$headers = array(
			    'Content-Type' => 'text/plain; charset=utf-8',
			    'Content-Transfer-Encoding' => 'base64',
			    'From' => 'cap+scenari@utc.fr',
			    'Reply-To' => 'cap+scenari@utc.fr',
			    'X-Mailer' => 'Scenari-Manager'
			);
			mail($to, $subject, $message, $headers);
		}
		error_log("SCENARI-MANAGER: succès de création de l'utilisateur '$login', suite à demande par '$creator'.");
	} elseif ($reta['result']=='failedAccountConflict') {
		$status.="Utilisateur \"$login\" déjà existant\n";
		$error=false;
	} else {
		$status.="Erreur interne d'activation ou d'identification pour l'utilisateur \"$login\".\n";
		$error=true;
	}
} else {
	$status.="Erreur de création ou d'identification pour l'utilisateur \"$login\".\n";
	$error=true;
}

// test server : 001MDudTHTy2S1MCg2H5uw
// we01 prod : 0252OxMD49wCQ0n67bZs4W

/*
// give rights on existing workshop
$ret=callAPI('GET', "/web/u/wspSrc",
array(
	'cdaction'=>'SetSpecifiedRoles',
	'param'=>'0252OxMD49wCQ0n67bZs4W', // workshop id
	'refUri'=>'',
	'options'=>"($login(allowedRoles*'main:author'-deniedRoles*-))"
));
usleep(500000);
}*/

?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="fui/semantic.css">
		<script src="fui/semantic.js"></script>
	</head>
	<body>
		<div class="ui main text container">
		<div class="ui segments">
<?php
if ($status) {
?>
			<article class="ui segment">
				<h2 class="ui header">Résultat de l'oppération</h2>
				<p><pre><?php echo $status;?></pre>
				</p>
<?php if ($error==false) {
	echo '<p>vous pouvez maintenant <a href="https://scenariutc.utc.fr/scenari5/~~static/fr-FR/home.xhtml">vous connecter au SCENARIserver</a> avec votre compte UTC</p>';
	echo '<p>La version actuelle de SCENARIserver 5 est compatible seulement avec les navigateurs basé sur le moteur webkit (chrome, chromium, brave...)</p>';
} else {
	error_log("SCENARI-MANAGER: erreur de création de l'utilisateur '$login', suite à demande par '$creator'.");
}
?>
			<p><a href="./">Retour à l'accueil de la gestion des comptes</a></p>
			</article>
<?php
}
?>
			<!--article class="ui segment">
				<h2 class="ui header">Créer un atelier et un dépôt</h2>
				<p>La création de documents Scenari se passe dans un atelier. Quand créer un atelier ?
				<ul>
					<li>Un atelier par groupe de personnes : avoir un atelier dédié permet de restreindre plus facilement le partage aux contenus de cet atelier.</li>
					<li>Un atelier par projet ou famille de projets : pour réutiliser des fragments de contenus entre les différents documents, regroupez les documents d'une même thématique dans un même atelier.</li>
					<li>Un atelier personnel : si vous avez des documents pour lesquels vous ne collaborez avec personne, vous pouvez les rassembler dans un atelier en votre nom.</li>
					<li>Un atelier par modèle documentaire : si vous travaillez sur 2 types de documents distincts (document pédagogique + site de présentation du projet), vous pouvez avoir besoin d'utiliser plusieurs modèles documentaire, et donc de créer des ateliers supplémentaires (chaque atelier doit être associé à un modèle documentaire).</li>
					<li>L'atelier que vous souhaitez n'existe pas déjà : si vous aviez été invité à travailler dans un projet qui as déjà des ateliers, il ne serait pas nécessaire d'en créer de nouveaux.</li>
				</ul>
				</p>
				<a class="ui fluid submit button" href="manager.php">Créer l'atelier</a>
			</article-->
		</div>
		</div>
	</body>
</html>
